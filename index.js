fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(response => {
    response.map(element => {
        console.log(element.title)
    });
})

fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then(data => data.json() )
.then(data => {
    console.log(data)
    console.log(`The item ${data.title} on the list has a status of ${data.completed}`)

})

fetch(`https://jsonplaceholder.typicode.com/todos`, {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: false,
        title: "Created to do list item",
        userId: 1
    })
})
.then( response => response.json() )
.then( response => {
    console.log(response)
})

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated to do list item",
        description: "to update my to do list item with different data structure",
        status: "Pending",
        dateCompleted: "Pending",
        userId: 1,
        id: 1
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        status: "Complete",
        dateCompleted: "07/09/21",
    })
})
.then( data => data.json())
.then(data => {
    console.log(data)
})

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "DELETE"
})

